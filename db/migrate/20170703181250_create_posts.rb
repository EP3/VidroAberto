class CreatePosts < ActiveRecord::Migration[5.1]
  def change
    create_table :posts do |t|
      t.string :carro
      t.text :mensagem
      t.date :data

      t.timestamps
    end
  end
end
